package adder

import (
	"context"
	"hobby/grpcadder/pkg/api"
)

// GRPCServer ...
type GRPCServer struct {
}

// Add ...
func (s *GRPCServer) Add(_ context.Context, req *api.AddRequest) (*api.AddResponse, error) {
	return &api.AddResponse{Result: req.GetX() + req.GetY()}, nil
}
